# Dracula Theme for R Markdown based beamer presentations.

---


|Default ![](Screenshot_1.png)  | PaloAlto ![](Screenshot_2.png)  |
|-----------------|-----------------|
|CambridgeUS ![](Screenshot_3.png)  |Ilmenau ![](Screenshot_4.png)  |
|Warsaw ![](Screenshot_5.png)  |Antibes ![](Screenshot_6.png) 

## How to Use 

- Download `beamercolorthemedracula.sty` file from this repository 
- Add the `.sty` file to your projects root directory 
- add `colortheme: "dracula"` to YAML header
   
```yaml
---
title: "A Dracula Theme for Beamer Presentation"
subtitle: "Using R Markdown"
author: |
   | Your Name (Msc) 
   | example@gmail.com
institute: "Your instiution Name"
date: "1/16/2021"
output: 
  beamer_presentation:
    theme: "default"
    colortheme: "dracula"
    slide_level: 3
    incremental: true
---
```